import json

from django.shortcuts import render
from django.http import HttpResponse
import requests
from django.views.decorators.csrf import csrf_exempt


def index(request):
    return render(request, 'index.html')


def firebase(request):
    return render(request, 'firebase-messaging-sw.js', content_type='text/javascript')


# def showFirebaseJS(request):
#     data = 'importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");' \
#            'importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js"); ' \
#            'var firebaseConfig = {' \
#            '        apiKey: "AIzaSyC6kncH-_ky3v61r2h4tH49nY5O-gciKhw",' \
#            '        authDomain: "fcm-test-7b30a.firebaseapp.com",' \
#            '        projectId: "fcm-test-7b30a",' \
#            '        storageBucket: "fcm-test-7b30a.appspot.com",' \
#            '        messagingSenderId: "886295795299",' \
#            '        appId: "1:886295795299:web:75211324829a92f6a9ca56",' \
#            '        measurementId: "G-4X7K0B9HQL"' \
#            ' };' \
#            'firebase.initializeApp(firebaseConfig);' \
#            'const messaging=firebase.messaging();' \
#            'messaging.setBackgroundMessageHandler(function (payload) {' \
#            '    console.log(payload);' \
#            '    const notification=JSON.parse(payload);' \
#            '    const notificationOption={' \
#            '        body:notification.body,' \
#            '        icon:notification.icon' \
#            '    };' \
#            '    return self.registration.showNotification(payload.notification.title,notificationOption);' \
#            '});'
#
#     return HttpResponse(data, content_type="text/javascript")

@csrf_exempt
def send(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        body = request.POST.get('body')
        headers = {
            'Authorization': 'key=AAAAWZIh-QQ:APA91bEfVjKs39Hy4KHhuZvKtxiXLIC_z5bT0f_4o7FWCOadNM9P4u51CQKcHPMHW63emoSCvbxstHBIf1uYEWRic8jOQzeSUUfOVBaXWvZWO4NSIP6wdaxAEE9fpuUzwmJhTP8ADbXS',
            # Already added when you pass json= but not when you pass data=
            # 'Content-Type': 'application/json',
        }

        json_data = {
            'to': 'eUhT6c54E2jpQtJVPEEPBz:APA91bFmbFOYHuub6nQMf6-4Wz_AGScIqB8aOtxuVAG66wBFxSmkbreryUv2nK-Kmr8i__nbjCUk7NNGYVivF41rSEh6NWzeOhSa8FjmzWnMmg_PlV75GCtAjeWXNBR9JuqaZK5XrWnq',
            'notification':
                {
                    'body': body, 'title': title
                }
        }

        response = requests.post('https://fcm.googleapis.com/fcm/send', headers=headers, json=json_data)
    return HttpResponse('ok')
