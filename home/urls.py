from django.contrib import admin
from django.urls import path, include

from home.views import index, firebase, send

urlpatterns = [
    path('', index, name='index'),
    path('firebase-messaging-sw.js/', firebase),
    path('send/', send, name='send')
]